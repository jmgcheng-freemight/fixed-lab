$(document).ready(function(){

	/**/
	$('#js-nav-sp-trigger').on('click', function(e){
		e.preventDefault();
		$('#js-nav-sp').toggleClass('is-shown');
	});
	$('main, footer').click(function() {
		try {
			$('#js-nav-sp').removeClass('is-shown');
		}
		catch(e) {}
	});
	$(window).resize(function(){
	    try {
			$('#js-nav-sp').removeClass('is-shown');
		}
		catch(e) {}
	});


	/**/
	$('.js-scrolltrig').on('click', function(e){
		e.preventDefault();
		//
		$('#js-nav-sp').removeClass('is-shown');
		//
		s_target = $(this).attr('href');
		i_scroll_adj = 0;
		if( $( window ).width() > 768 )
		{
			i_scroll_adj = 110;
		}
		else if( $( window ).width() <= 768 )
		{
			i_scroll_adj = 71;
		}
		$('html, body').animate({
	        scrollTop: ( $(s_target).offset().top ) - 110
	    }, 1000);
	});


	/**/
	$('.faq-list-qa-q-anc').on('click', function(e){
		e.preventDefault();
		o_parent = $(this).parents('.faq-list-qa-q');
		o_target = o_parent.siblings('[class="faq-list-qa-a"]');
		o_target.slideToggle();		
	});

});
